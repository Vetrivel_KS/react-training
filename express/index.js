const express = require('express')

const app = express()
// checks this first ,
// if you declare app.get('*' before everything else , 
// even if  you hit localhost:5888/test .. it will result in 'everything else success' 
app.get('/test',(req, res) => {
    res.status(200).json({ body: 'test success' })
})

app.get('*',(req, res) => {
    res.status(200).json({ body: 'everything else success' })
})
// app.post
// // app.options
// // app.delete
// // app.patch
// // app.put

// app.use


app.listen(5888, () => console.log('listening at localhost:5888'))
