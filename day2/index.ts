const app = {
    eh: '2.0'
};

const addCheck = (inputOne:number, inputTwo:number): number => {
    return inputOne+inputTwo
}

console.log('vetri_add',addCheck(1,2))
interface addInputType {
    inputOne: number;
    inputTwo: number;
}

function anotherAdd(config: addInputType): {addResult: number} {
    return {
        addResult: config.inputOne + config.inputTwo
    };
}

console.log('vetri_addAnother',anotherAdd({inputOne: 1,inputTwo: 2}))
// tried object destructuring
const { addResult } = anotherAdd({inputOne: 4,inputTwo: 5})
console.log('addResult',addResult)
