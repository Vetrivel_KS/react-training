# react_training
## Repo created during react training 2.0



NOTES:
day 1:
prepublish lifecycle

 somevar --> not a file path

require(somevar)

-checks for node modules in the directory,
not found means it traverses to parent directory
not found means it traverses to parent directory
not found means it traverses to parent directory
.....


express => chainable-ish API

middleware

express server has specific order 
/vetri
*



PLAN:
1. **the beginning: package**
## Zero
- [ ] editor: screenshots of your plugins
## One
> init
### Steps
1. [create a repo](https://bitbucket.org/repo/create)
2. [yarn init](https://yarnpkg.com/lang/en/docs/creating-a-project/)

 git init
git remote add origin xxxxxxxxxxx_your_link_to_clone_bitbucket

3. [create an npm account](https://docs.npmjs.com/getting-started/publishing-npm-packages)
4. [create a github account](https://github.com/join?source=header-repo)
5. [sign into code sandbox](https://codesandbox.io/auth/github)
6. [create a `Makefile`]
7. [create an `index.js`]
8. [create a test.js file]
9. [run the test file with a makefile script]
10. [call that makefile script from package json scripts]
### Expected output
```
- package.json
    - scripts: { test, dev }
- .git
- yarn.lock
- Makefile
    - dev
    - test
- index.js
- test.js
```
### Tasks
#### Reference Store
- [ ] create a package (_submit_)
- [ ] create a code sandbox
## Two
> _/course/yarn_
### Tasks
#### Reference Store
- [ ] what is yarn for?
- [ ] what is codesandbox for?
## Three
> stuff from 1 & 2
## EOD
- [ ] link to repo
- [ ] submit 2+ questions

